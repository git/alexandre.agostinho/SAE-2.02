/**
 * SAE 2.02 - Exploration algorithmique d’un problème
 * Prtie 1 / Exercice 1
 * Extrait code C
 */


#define NAME_MAX_LEN 40
#define NB_PTS_CARACT_MAX 10

typedef struct pointCaracteristique {
    char nom[NAME_MAX_LEN];
    struct pointCaracteristique **tPtsCaract;
    int nbPtsCaract;
} PointCaracteristique, *Rue;

PointCaracteristique *reseau[NB_PTS_CARACT_MAX];
