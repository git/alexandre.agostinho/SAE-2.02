/**
 * SAE 2.02 - Exploration algorithmique d’un problème
 * Prtie 1 / Exercice 4
 * Extrait code C
 */

#define NAME_MAX_LEN 40
#define NB_PTS_CARACT_MAX 10

typedef struct pointCaracteristique {
    char nom[NAME_MAX_LEN];
    struct pointCaracteristique **tPtsCaract;
    int nbPtsCaract;
} PointCaracteristique, *Rue;

PointCaracteristique *reseau[NB_PTS_CARACT_MAX];


// BUT: Savoir si B est directement accessible depuis A

int isAccessDirect(PointCaracteristique *A, PointCaracteristique *B)
{
    for (int i = 0; i < A->nbPtsCaract; i++)
    {
        if (A->tPtsCaract[i] == B)
        {
            return 1;
        }
    }
    return 0;
}


// BUT: Savoir si B est accessible depuis A

// version recusrive
int isAccessREC(PointCaracteristique *A, PointCaracteristique *B)
{
    for (int i = 0; i < A->nbPtsCaract; i++)
    {
        if (isAccessDirect(A, B))
        {
            return 1;
        }
        else if (isAccessREC(A->tPtsCaract[i], B))
        {
            return 1;
        }
    }
    return 0;
}

// version iterative (ebauche de travail)
int isAccessITE(PointCaracteristique *A, PointCaracteristique *B)
{
    int i = 0;

    PointCaracteristique *ptsMarque[NB_PTS_CARACT_MAX];
    int nbPtsMarque = 0;

    PointCaracteristique *ptsTest = A;

    // pour tout les points du point teste
    for (i = 0; i < ptsTest->nbPtsCaract; i++)
    {
        // test si point deja teste
        for (int j = 0; j < nbPtsMarque; j++)
        {
            if (ptsTest == ptsMarque[j])
            {
                break;
            }
        }

        // test si pour ce point le chemin est direct
        if (isAccessDirect(ptsTest, B))
        {
            return 1;
        }

        // le point viens d etre teste, il entre dans la liste
        ptsMarque[nbPtsMarque++] = ptsTest;

        // on teste ensuite le prochain
        ptsTest = ptsTest->tPtsCaract[i];
    }

    return 0;
}

int isInTab(PointCaracteristique *P, PointCaracteristique **TabP, int lenTabP)
{
    for (int i; i < lenTabP; i++)
    {
        if (TabP[i] == P)
        {
            return 1;
        }
    }
    return 0;
}

// version rapide
int isAccess(PointCaracteristique *A, PointCaracteristique *B, PointCaracteristique **ptsMarque, int *nbPtsMarque)
{
    for (int i = 0; i < A->nbPtsCaract; i++)
    {
        if (! isInTab(A, ptsMarque, *nbPtsMarque))
        {
            ptsMarque[*nbPtsMarque++] = A;
            if (isAccessDirect(A, B))
            {
                return 1;
            }
            else if (isAccess(A->tPtsCaract[i], B, ptsMarque, nbPtsMarque))
            {
                return 1;
            }
        }
    }
    return 0;
}
