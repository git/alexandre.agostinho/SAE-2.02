/**
 * SAE 2.02 - Exploration algorithmique d’un problème
 * Prtie 2 / Exercice 1
 * Extrait code C
 */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

/// - ALGO 1 -----------------------------------------------

void echanger(int tab[], int i, int j) {
    int tmp;
    tmp = tab[i];
    tab[i] = tab[j];
    tab[j] = tmp;
}

void algo1(int tab[], int nb) {
    int echange = 1;
    int i;
    int deb = 0, fin = nb-1;
    while (echange > 0) {
        echange = 0;
        for(i = deb; i < fin; i++) {
            if(tab[i] > tab[i+1]) {
                echanger(tab, i, i+1);
                echange++;
            }
        }
        fin--;
        for(i = fin; i > deb; i--) {
            if(tab[i] < tab[i-1]) {
                echanger(tab, i, i-1);
                echange++;
            }
        }
        deb++;
    }
}


/// - ALGO 2 -----------------------------------------------

void algo2(int tab[], int nb) {
    int* cpt;
    int min = tab[0], max = tab[0];
    int i, j;
    for(i = 1; i < nb; i++) {
        if(min > tab[i]) min = tab[i];
        if(max < tab[i]) max = tab[i];
    }
    cpt = (int *) calloc((max-min+1), sizeof(int));
    if(cpt == NULL) {
        printf("pb aloc mémoire \n");
        exit(1);
    }
    for(i = 0; i < nb; i++) {
        cpt[tab[i]-min]++;
    }
    j = 0;
    for(i = 0; i < max-min+1; i++) {
        while (cpt[i] != 0) {
            tab[j] = i + min;
            j++;
            cpt[i]--;
        }
    }
    free(cpt);
}


/// - ALGO 3 -----------------------------------------------

int recherchePos(int tab[], int n, int val) {
    int i;
    for(i = 0; i < n; i++) {
        if(val < tab[i]) {
            return i;
        }
    }
    return i;
}

void algo3(int tab[], int n) {
    int i, pos, j, tmp;
    for(i = 1; i < n; i++) {
        pos = recherchePos(tab, i, tab[i]);
        if(pos != i){
            tmp = tab[i];
            for(j = i; j > pos; j--) {
                tab[j] = tab[j-1];
            }
            tab[pos] = tmp;
        }
    }
}


/// - MAIN --------------------------------------------------

void prtab(int tab[], int n) {
    for (int i = 0; i < n; i++)
        printf("%d ", tab[i]);
}

void tstalgo(char *title, void (*algo)(int[], int), int tab[], int n) {

    double time_spent = 0.0;
    clock_t begin = clock();

    (*algo)(tab, n);

    clock_t end = clock();
    time_spent += (double)(end - begin) / CLOCKS_PER_SEC;

    printf("(%f) ", time_spent);
    printf("%s: ", title);
    prtab(tab, n);
    printf("\n");
}

int foo() { return 1; }

int main(void) {

#define TABLEN 6
    int tab1[] = {13, 22, 1, 4, 9, 10};
    int tab2[] = {1, 2, 4, 6, 9, 10};
    int tab3[] = {111, 24, 4, 4, 2, 1};

    printf("\n[ TEST ALGO 1 ]:\n");
    {   // test algo 1
        int *t1 = tab1, *t2 = tab2, *t3 = tab3;
        tstalgo("al1t1", algo1, t1, TABLEN);
        tstalgo("al1t2", algo1, t2, TABLEN);
        tstalgo("al1t3", algo1, t3, TABLEN);
    }

    printf("\n\n[ TEST ALGO 2 ]:\n");
    {   // test algo 2
        int *t1 = tab1, *t2 = tab2, *t3 = tab3;
        tstalgo("al2t1", algo2, t1, TABLEN);
        tstalgo("al2t2", algo2, t2, TABLEN);
        tstalgo("al2t3", algo2, t3, TABLEN);
    }

    printf("\n\n[ TEST ALGO 3 ]:\n");
    {   // test algo 3
        int *t1 = tab1, *t2 = tab2, *t3 = tab3;
        tstalgo("al3t1", algo3, t1, TABLEN);
        tstalgo("al3t2", algo3, t2, TABLEN);
        tstalgo("al3t3", algo3, t3, TABLEN);
    }
}
